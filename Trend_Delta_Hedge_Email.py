from datetime import datetime
from IPython.core.display import display, HTML
import numpy as np
import pandas as pd
from pandas.tseries.offsets import BDay
import matplotlib.cm as cm
import time
import pickle

import operator
import itertools
from scipy import stats
from scipy.stats import percentileofscore, skew, kurtosis

from vol_tools.vtio.data_manager import BbgDataManager, SidAccessor, MultiSidAccessor
from vol_tools.utils.utils import offset2datetime
from vol_tools.quant.volatility import sget_vol_ext

np.set_printoptions(precision=2, linewidth=260)
pd.set_option('display.precision', 2)
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from itertools import compress

class MaskableList(list):
    def __getitem__(self, index):
        try: return super(MaskableList, self).__getitem__(index)
        except TypeError: return MaskableList(compress(self, index))

def cc_estimator(sample,n=22):
    sample_clean = sample.dropna()
    returns = np.divide(sample_clean[1:],sample_clean[:-1])
    log_returns = np.log(returns)
    ann_log_returns = 252*np.power(log_returns,2)/n
    return 100 * np.sqrt(ann_log_returns.rolling(window=n,min_periods=n).sum())

def calc_period_var(sample, return_period=22, lookback=66):
    sample_clean = sample.dropna()
    lookback_ret = sample_clean.pct_change(periods=return_period)
    return (lookback_ret**2).rolling(window=lookback).mean() * 250 / return_period

def calc_var_ratio(sample, return_period=22, period_min=3, day_min=66):
    lookback = min(return_period * period_min, day_min)
    period_var = calc_period_var(sample, return_period=return_period, lookback=lookback)
    daily_var = calc_period_var(sample, return_period=1, lookback=lookback)
    return period_var / daily_var

def calc_lfev(sample, return_period=22, period_min=3, day_min=66):
    lookback = min(return_period * period_min, day_min)
    period_var = calc_period_var(sample, return_period=return_period, lookback=lookback)
    daily_var = calc_period_var(sample, return_period=1, lookback=lookback)
    return (np.sqrt(period_var) - np.sqrt(daily_var)) * 100

def past_spot_ranges(sample, n=22, haircut=0.2):
    '''Finds Returns the past n spot range based on max/min of the period'''
    sample_max = (sample['PX_HIGH'].rolling(n).max() / sample['PX_LAST'].shift(n) - 1) * 100
    sample_min = (sample['PX_LOW'].rolling(n).min() / sample['PX_LAST'].shift(n) - 1) * 100
#     if n == 1:
#         return abs(sample_max) / 2
    # return (sample_max - sample_min) / 2
    delta_scale = 1 - haircut # Set a more conservative estimate of the range.
    return pd.concat([abs(sample_max) * delta_scale, abs(sample_min) * delta_scale], axis=1).max(axis=1) 

def rolling_trend(returns_dict, undl_list, return_days, smoothing=5):
    '''Determines the trend by blending the returns across different periods and smooths the results.'''
    avg_returns_dict = {}
    returns_summary = {}
    for u in undl_list:
        avg_returns_dict[u] = pd.DataFrame()
        for i in return_days:
            avg_returns_dict[u][i] = returns_dict[u][i].dropna().rolling(smoothing).mean() / np.sqrt(i)
        avg_returns_dict[u]['Average'] = avg_returns_dict[u].dropna().mean(axis=1)
        if len(avg_returns_dict[u].dropna()) > 0:
            returns_summary[u] = avg_returns_dict[u]['Average'].dropna()[-1]
    returns_summary = pd.Series(returns_summary)
    return returns_summary, avg_returns_dict

def spot_stats(sample, n=260):
    spot_window = sample['PX_LAST'].dropna()[-n:]
    percentile = percentileofscore(spot_window, spot_window[-1])
    high = spot_window.max()
    low = spot_window.min()
    max_pct = (high / spot_window[-1] - 1) * 100
    min_pct = (low / spot_window[-1] - 1) * 100
    return max_pct, min_pct, percentile

def get_live_ma_deals(deal_data, undl, min_deal_size = 1e10):
    if undl in deal_data.index:
      undl_deal_data = deal_data.loc[undl]['MERGERS_AND_ACQUISITIONS']
      if isinstance(undl_deal_data, pd.DataFrame):
          live_deal = (undl_deal_data['Deal Status'] == 'Pending') | (undl_deal_data['Deal Status'] == 'Proposed')
          deal_type = (undl_deal_data['Deal Type'] == 'M&A')
          # Min deal size does not convert the currency.
          deal_value = (undl_deal_data['Announced Total Value'] >= min_deal_size)
          return undl_deal_data[live_deal & deal_type & deal_value]
    return pd.DataFrame()

def series_mean(s):
    return s.mean()

def series_median(s):
    return s.median()

def main():

    '''parameters below: underliers, dates, and returns in days to calculate trend'''
    index_undl = [
           'HSI Index',
           'HSCEI Index',
           'NKY Index',
           'KOSPI2 Index',
           'AS51 Index',
           'SPX Index',
           'NDX Index',
           'RTY Index',
           'SX5E Index',
           'UKX Index',
           'DAX Index']

    asia_stock_undl = ['2317 TT Equity',
           '000660 KS Equity',
           '3380 HK Equity',
           '257 HK Equity',
           '27 HK Equity',
           '3008 TT Equity',
           'ST SP Equity',
           'DBS SP Equity',]

    current_positions = ['HSI Index',
          'NKY Index', 
          'KOSPI2 Index',
          'SPX Index',
          '2317 TT Equity',
           '000660 KS Equity',
           '257 HK Equity',
           '2318 HK Equity',
           '3988 HK Equity',
           'PAG US Equity',
           '8316 JT Equity',
           '6902 JT Equity',
           'MQG AT Equity',
           'RIO AT Equity']
    
    sg_stocks = list(pd.read_csv('sg_stock_list.csv')['Quotable Stock List'])

    asia_stock_undl = asia_stock_undl + sg_stocks

    us_stock_undl = list(pd.read_csv('spx_stock_list.csv')['Quotable Stock List'])
    europe_stock_undl = list(pd.read_csv('europe_stock_list.csv')['Quotable Stock List'])


    # auto_corr_len = [1, 2, 3, 5, 10, 22, 66, 130, 260]
    auto_corr_len = [5, 10, 22, 66, 130, 260]
    spot_ranges_days = [1, 2, 3, 5, 10, 22, 66]
    vol_slope_day = '5D' # Day Frequency used to calculate the slope of the vol bucketing.
    low_vol_cutoff = 2.5 # Decile cutoff for low vol

    undl = index_undl + asia_stock_undl + current_positions 
    internal_undl = [x[:-(len(x.split(' ')[-1]) + 1)] for x in undl]
    undl = undl + us_stock_undl + europe_stock_undl
    undl_dict = {x: x[:-(len(x.split(' ')[-1]) + 1)] for x in undl for x in undl} #map from bbg to internal vol db undl

    start_date_list_summary = [pd.Timestamp('2000-01-01'), pd.Timestamp('2008-01-01'), pd.Timestamp('2012-01-01'), pd.Timestamp('2015-01-01'), pd.Timestamp('2017-01-01'), pd.Timestamp('2018-01-01')]
    end_date = pd.Timestamp.now().normalize()

    start_date = offset2datetime('20Y', end_date, forward=False)
    end_date = pd.Timestamp.now().normalize()


    ''' Get data from either pickle files or from bloomberg and internal vol database. Use the price data to generate trends and spot trading ranges, or read from pickle files.'''  
    print('Gathering price and vol data')
    start_time = time.time()
    try:
        prices = pickle.load(open('BBG_Internal_Historical/bbg_historical{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
        ma_deals = pickle.load(open('BBG_Internal_Historical/bbg_ma_deals{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
        bbg_to_name_dict = pickle.load(open('BBG_Internal_Historical/bbg_to_name{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
        print('Pulled from BBG Data from pickle file')
    except (OSError, IOError) as e:
        bbg = MultiSidAccessor(undl, BbgDataManager())
        prices = bbg.get_historical(['PX_LAST', 'PX_HIGH', 'PX_LOW', 'PX_OPEN', 
          '12MTH_IMPVOL_90.0%MNY_DF', '12MTH_IMPVOL_110.0%MNY_DF', '6MTH_IMPVOL_90.0%MNY_DF', '6MTH_IMPVOL_110.0%MNY_DF'], start_date, end_date)

        prices = prices.rename(columns={'12MTH_IMPVOL_110.0%MNY_DF': '12M 110 IV', '6MTH_IMPVOL_110.0%MNY_DF': '6M 110 IV', 
          '12MTH_IMPVOL_90.0%MNY_DF': '12M 90 IV', '6MTH_IMPVOL_90.0%MNY_DF': '6M 90 IV',})
        for u in undl:
            prices[u, '12M IV'] = 1/2 * (prices[u]['12M 90 IV'] + prices[u]['12M 110 IV'])
            prices[u, '6M IV'] = 1/2 * (prices[u]['6M 90 IV'] + prices[u]['6M 110 IV'])
            prices[u, 'Blend IV'] = 1/2 * (prices[u]['12M IV'] + prices[u]['6M IV'])
            prices[u, '10D RV'] = cc_estimator(prices[u]['PX_LAST'], n=10)
            prices[u, '22D RV'] = cc_estimator(prices[u]['PX_LAST'], n=22)
            prices[u, 'Future 10D RV'] = prices[u, '10D RV'].shift(-10)
            prices[u, 'Future 22D RV'] = prices[u, '22D RV'].shift(-22)
            prices[u, 'LFEV 10D'] = calc_lfev(prices[u, 'PX_LAST'], return_period=10, period_min=3, day_min=66)
            prices[u, 'LFEV 22D'] = calc_lfev(prices[u, 'PX_LAST'], return_period=22, period_min=3, day_min=66)
            prices[u, 'Var Ratio 10D'] = calc_var_ratio(prices[u, 'PX_LAST'], return_period=10, period_min=3, day_min=66)
            prices[u, 'Var Ratio 22D'] = calc_var_ratio(prices[u, 'PX_LAST'], return_period=22, period_min=3, day_min=66)


        ma_deals = bbg.get_attributes('MERGERS_AND_ACQUISITIONS', ignore_field_error=1, ignore_security_error=1)
        bbg_to_name_dict = bbg.get_attributes(['DS192'], ignore_field_error=1, ignore_security_error=1)['DS192'].to_dict()

        pickle.dump(bbg_to_name_dict, open('BBG_Internal_Historical/bbg_to_name{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))
        pickle.dump(ma_deals, open('BBG_Internal_Historical/bbg_ma_deals{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))
        pickle.dump(prices, open('BBG_Internal_Historical/bbg_historical{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))

    print('Seconds to get BBG Data: {}'.format(round((time.time() - start_time))))

    start_time = time.time()
    try:
        vols = pickle.load(open('BBG_Internal_Historical/internal_vols_historical{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
    except (OSError, IOError) as e:
        vols = sget_vol_ext(internal_undl, start_date, end_date, ['6M', '12M'], [90, 110])
        for u in internal_undl:
          if u in vols.columns.levels[0]:
              vols[u, '12M', 100] = 1/2 * (vols[u, '12M', 90] + vols[u, '12M', 110])
              vols[u, '6M', 100] = 1/2 * (vols[u, '6M', 90] + vols[u, '6M', 110])
              vols[u, 'Blend', 100] = 1/2 * (vols[u, '12M', 100] + vols[u, '6M', 100])
        pickle.dump(vols, open('BBG_Internal_Historical/internal_vols_historical{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))

    print('Seconds to get Internal Vol Data: {}'.format(round((time.time() - start_time))))

    start_time = time.time()
    try:
        r_dict = pickle.load(open('BBG_Internal_Historical/r_dict{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
        r_summary = pickle.load(open('BBG_Internal_Historical/r_summary{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
    except (OSError, IOError) as e:
        returns = {}
        for u in undl:
            returns[u] = pd.DataFrame()
            for i in auto_corr_len:
                close_prices = prices[u, 'PX_LAST'].dropna()
                returns[u][i] = (close_prices / close_prices.shift(i) - 1) * 100
        r_summary, r_dict = rolling_trend(returns, undl, auto_corr_len)
        r_summary.sort_values()
        pickle.dump(r_dict, open('BBG_Internal_Historical/r_dict{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))
        pickle.dump(r_summary, open('BBG_Internal_Historical/r_summary{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))
    print('Seconds to get Spot Trends: {}'.format(round((time.time() - start_time))))

    start_time = time.time()
    try:
        spot_ranges = pickle.load(open('BBG_Internal_Historical/spot_ranges{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'rb'))
    except (OSError, IOError) as e:
        spot_ranges = {}
        for u in undl:
            spot_ranges[u] = pd.DataFrame()
            for i in spot_ranges_days:
                spot_ranges[u]['Past {}D'.format(i)] = past_spot_ranges(prices[u][['PX_LAST', 'PX_HIGH', 'PX_LOW', 'PX_OPEN']].dropna(), n=i) / np.sqrt(i)
                spot_ranges[u]['Future {}D'.format(i)] = (past_spot_ranges(prices[u][['PX_LAST', 'PX_HIGH', 'PX_LOW', 'PX_OPEN']].dropna(), n=i) / np.sqrt(i)).shift()
        pickle.dump(spot_ranges, open('BBG_Internal_Historical/spot_ranges{}.p'.format(pd.Timestamp.now().normalize().strftime('%Y%m%d')), 'wb'))
    print('Seconds to get Spot Ranges: {}'.format(round((time.time() - start_time))))

    num_slices = 10
    summary_funct = series_median
    iv_slice_mat = 'Blend'
    
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    sender_email = "vol@three-stones.com"  # Enter your address
    receiver_email = ['payton.ong@three-stones.com', 'jongbeum.kim@three-stones.com', 'yiyong.feng@three-stones.com', 'Zhelei.li@three-stones.com']  # Enter receiver address
    # receiver_email = ["payton.ong@three-stones.com"]  # Enter receiver address
    password = "ECP-K4a-qs2-5k8"

    for undl_type in [current_positions, index_undl, asia_stock_undl, us_stock_undl, europe_stock_undl]:
    # for undl_type in [current_positions, index_undl, asia_stock_undl, us_stock_undl]:
        msg = MIMEMultipart('alternative')
        msg['From'] = sender_email
        msg['To'] = ', '.join(receiver_email)
        if undl_type == index_undl:
            # msg['Subject'] = 'Index Trend Summary {}'.format(end_date.strftime('%D'))
            msg['Subject'] = 'Index Trend Summary {}'.format(pd.Timestamp.now())
        elif undl_type == asia_stock_undl:
            # msg['Subject'] = 'Stock Trend Summary {}'.format(end_date.strftime('%D'))
            msg['Subject'] = 'Asia Stock Trend Summary {}'.format(pd.Timestamp.now())
        elif undl_type == us_stock_undl:
            msg['Subject'] = 'US Stock Trend Summary {}'.format(pd.Timestamp.now())
        elif undl_type == europe_stock_undl:
            msg['Subject'] = 'Europe Stock Trend Summary {}'.format(pd.Timestamp.now())
        elif undl_type == current_positions:
            msg['Subject'] = 'Current Positions Trend Summary {}'.format(pd.Timestamp.now())
        messageHTML = ''
        messagePlain = 'Trend Summary'
        messageSummaryHTML = ''
        msg.attach(MIMEText(messagePlain, 'plain'))

        iv_to_trend_range_comp_df = pd.DataFrame()
        iv_to_iv_range_comp_df = pd.DataFrame()
        iv_to_trendvol_range_comp_df = pd.DataFrame()
        iv_to_voltrend_range_comp_df = pd.DataFrame()
        iv_to_spot_range_comp_df = pd.DataFrame()
        vol_slope_dict = {}
        low_vol_list = []

        for u in undl_type:
            summary_trend_df = pd.DataFrame()
            summary_vol_df = pd.DataFrame()
            summary_trendvol_df = pd.DataFrame()
            summary_voltrend_df = pd.DataFrame()
            vol_slope_df = pd.DataFrame()

            # Not enough returns to generate a trend
            if (u not in r_dict) or (len(r_dict[u]['Average'].dropna()) < 400):
                print('{} does not have enough returns for a trend'.format(u))
                continue

            current_trend = r_dict[u]['Average'].dropna()[-1]
            bbg_vol_index = prices[u]['Blend IV'].dropna().index
            vol_source = 'BBG'

            internal_vol_size = 0
            if undl_dict[u] in vols.columns.levels[0]:
                internal_vol_size = len(vols[undl_dict[u], iv_slice_mat, 100].dropna())
            bbg_vol_size = len(prices[u]['{} IV'.format(iv_slice_mat)].dropna())

            high_pct, low_pct, pctile = spot_stats(prices[u], n=260)

            if ((len(bbg_vol_index) > 0) and ((end_date - bbg_vol_index[-1]).days > 10)) or (internal_vol_size >= bbg_vol_size):
                vol_source = 'Internal'
                if undl_dict[u] in vols.columns.levels[0]:
                    current_vol = vols[undl_dict[u], iv_slice_mat, 100][-1]
                    current_vol_date = vols[undl_dict[u], iv_slice_mat, 100].index[-1]
                else:
                    current_vol = np.nan
            elif len(bbg_vol_index) > 0:
                current_vol = prices[u]['{} IV'.format(iv_slice_mat)].dropna()[-1]
                current_vol_date = prices[u]['{} IV'.format(iv_slice_mat)].dropna().index[-1]
            else:
                current_vol = np.nan

            print('Processing {}. {} Vol: {}'.format(u, vol_source, current_vol))
                
            lfev_10d = round(prices[u, 'LFEV 10D'][-1], 2)
            lfev_22d = round(prices[u, 'LFEV 22D'][-1], 2)
            var_ratio_10d = round(prices[u, 'Var Ratio 10D'][-1], 2)
            var_ratio_22d = round(prices[u, 'Var Ratio 22D'][-1], 2)

            for start_date in start_date_list_summary:
                trend_mask_dates = (r_dict[u].index >= start_date) & (r_dict[u].index <= end_date)
                trend_mask_spot_ranges = (spot_ranges[u].index >= start_date) & (spot_ranges[u].index <= end_date)
                
                if ~np.isnan(current_vol):
                    if vol_source == 'BBG':
                        vol_mask_dates = (prices[u].index >= start_date) & (prices[u].index <= end_date)
                    elif vol_source == 'Internal':
                        vol_mask_dates = (vols[undl_dict[u]].index >= start_date) & (vols[undl_dict[u]].index <= end_date)
                            
                quantile_df = pd.DataFrame()

                #Calculate Trend Bucketing Data Frame
                for i in range(num_slices):
                    slice_range = r_dict[u].loc[trend_mask_dates]['Average'].quantile([i/num_slices, (i+1)/num_slices])
                    slice_quantile = r_dict[u].loc[trend_mask_dates][(r_dict[u].loc[trend_mask_dates]['Average'] > slice_range.values[0]) & (r_dict[u].loc[trend_mask_dates]['Average'] <= slice_range.values[1])]           
                    trend_mask = slice_quantile.index

                    trend_boolean = ((current_trend > slice_quantile['Average'].min()) and (current_trend <= slice_quantile['Average'].max())) or ((i == 0) and (current_trend <= slice_quantile['Average'].min())) or ((i+1 == num_slices) and (current_trend > slice_quantile['Average'].max()))

                    if trend_boolean:
                        mask = trend_mask
                        trend_1d = summary_funct(spot_ranges[u]['Future 1D'].loc[mask].dropna())
                        trend_2d = summary_funct(spot_ranges[u]['Future 2D'].loc[mask].dropna())
                        trend_3d = summary_funct(spot_ranges[u]['Future 3D'].loc[mask].dropna())
                        trend_5d = summary_funct(spot_ranges[u]['Future 5D'].loc[mask].dropna())
                        trend_10d = summary_funct(spot_ranges[u]['Future 10D'].loc[mask].dropna())
                        rv_10d = summary_funct(prices[u, 'Future 10D RV'].loc[mask].dropna()) / np.sqrt(252)
                        rv_22d = summary_funct(prices[u, 'Future 22D RV'].loc[mask].dropna()) / np.sqrt(252)
                        range_dict = {'1D': trend_1d, '2D': trend_2d, '3D': trend_3d, '5D': trend_5d, '10D': trend_10d}
                        largest_range = max(range_dict.items(), key=operator.itemgetter(1))[0]
                        largest_range_dist = spot_ranges[u]['Future {}'.format(largest_range)].loc[mask].dropna()
                        stats_skew = stats.skew(largest_range_dist.values)
                        stats_kurt = stats.kurtosis(largest_range_dist.values)
                        trend_quantile = i
                        summary_trend_df = summary_trend_df.append(pd.DataFrame({'1D': trend_1d,
                                                                  '2D': trend_2d,
                                                                  '3D': trend_3d,
                                                                  '5D': trend_5d,
                                                                  '10D': trend_10d,
                                                                  'RV 10D': rv_10d,
                                                                  'RV 22D': rv_22d,
                                                                  'Biggest Range Skew': stats_skew,
                                                                  'Biggest Range Kurtosis': stats_kurt,       
                                                                  'Quantile': trend_quantile,
                                                                  'Count': len(spot_ranges[u]['Future 1D'].loc[mask].dropna())}, index=['{} - {}'.format(start_date, end_date)]))
                if ~np.isnan(current_vol):
                    vol_date_range_list = []
                    #Calculate Vol Bucketing Data Frame
                    for i in range(num_slices):
                        if vol_source == 'BBG':
                            slice_range = prices.loc[vol_mask_dates][u, '{} IV'.format(iv_slice_mat)].quantile([i/num_slices, (i+1)/num_slices])
                            slice_quantile = prices.loc[vol_mask_dates][(prices.loc[vol_mask_dates][u, '{} IV'.format(iv_slice_mat)] > slice_range.values[0]) & (prices.loc[vol_mask_dates][u, '{} IV'.format(iv_slice_mat)] <= slice_range.values[1])][u, '{} IV'.format(iv_slice_mat)]
                        else:
                            slice_range = vols.loc[vol_mask_dates][undl_dict[u], iv_slice_mat, 100].dropna().quantile([i/num_slices, (i+1)/num_slices])
                            vols_mask_df = vols[undl_dict[u], iv_slice_mat, 100].loc[vol_mask_dates].dropna()
                            slice_quantile = vols_mask_df[(vols_mask_df > slice_range.values[0]) & (vols_mask_df <= slice_range.values[1])].dropna()
                        vol_mask = slice_quantile.index

                        vol_boolean = ((current_vol > slice_quantile.min()) and (current_vol <= slice_quantile.max())) or ((i == 0) and (current_vol <= slice_quantile.min())) or ((i+1 == num_slices) and (current_vol > slice_quantile.max()))


                        if vol_boolean:
                            mask = vol_mask
                            vol_1d = summary_funct(spot_ranges[u]['Future 1D'].loc[mask].dropna())
                            vol_2d = summary_funct(spot_ranges[u]['Future 2D'].loc[mask].dropna())
                            vol_3d = summary_funct(spot_ranges[u]['Future 3D'].loc[mask].dropna())
                            vol_5d = summary_funct(spot_ranges[u]['Future 5D'].loc[mask].dropna())
                            vol_10d = summary_funct(spot_ranges[u]['Future 10D'].loc[mask].dropna())
                            rv_10d = summary_funct(prices[u, 'Future 10D RV'].loc[mask].dropna()) / np.sqrt(252)
                            rv_22d = summary_funct(prices[u, 'Future 22D RV'].loc[mask].dropna()) / np.sqrt(252)
                            range_dict = {'1D': vol_1d, '2D': vol_2d, '3D': vol_3d, '5D': vol_5d, '10D': vol_10d}
                            largest_range = max(range_dict.items(), key=operator.itemgetter(1))[0]
                            largest_range_dist = spot_ranges[u]['Future {}'.format(largest_range)].loc[mask].dropna()
                            stats_skew = stats.skew(largest_range_dist.values)
                            stats_kurt = stats.kurtosis(largest_range_dist.values)
                            vol_quantile = i
                            summary_vol_df = summary_vol_df.append(pd.DataFrame({'1D': vol_1d,
                                                                      '2D': vol_2d,
                                                                      '3D': vol_3d,
                                                                      '5D': vol_5d,
                                                                      '10D': vol_10d,
                                                                      'RV 10D': rv_10d,
                                                                      'RV 22D': rv_22d,
                                                                      'Biggest Range Skew': stats_skew,
                                                                      'Biggest Range Kurtosis': stats_kurt,       
                                                                      'Quantile': vol_quantile,
                                                                      'Count': len(spot_ranges[u]['Future 1D'].loc[mask].dropna())}, index=['{} - {}'.format(start_date, end_date)]))
                        if vol_mask.isin(spot_ranges[u].index).sum() > 0:
                            vol_date_range_list.append(summary_funct(spot_ranges[u]['Future {}'.format(vol_slope_day)].loc[vol_mask].dropna())) 
                        else:
                            vol_date_range_list.append(np.nan)     
                    #Calculate regression slope of the vol range.
                    vol_slope_x = MaskableList(list(range(num_slices)))
                    vol_slope_y = MaskableList(vol_date_range_list)
                    slope_mask = ~np.isnan(vol_slope_x) & ~np.isnan(vol_slope_y)
                    vol_range_slope = stats.linregress(vol_slope_x[slope_mask], vol_slope_y[slope_mask])[0]
                    vol_slope_df = vol_slope_df.append(pd.DataFrame({'{} Range Slope'.format(vol_slope_day): round(vol_range_slope, 4)}, index=['{} - {}'.format(start_date, end_date)]))

                    #Calculate Trend Bucketing then Vol Bucketing Data Frame
                    for i in range(num_slices):
                        sub_trend_df =r_dict[u].loc[trend_mask_dates] 
                        slice_range =  sub_trend_df['Average'].quantile([i / num_slices, (i + 1) / num_slices])
                        slice_quantile = sub_trend_df[(sub_trend_df['Average'] > slice_range.values[0]) & (sub_trend_df['Average'] <= slice_range.values[1])]           
                        trend_mask = slice_quantile.index
                       
                        trend_boolean = ((current_trend > slice_quantile['Average'].min()) and (current_trend <= slice_quantile['Average'].max())) or ((i == 0) and (current_trend <= slice_quantile['Average'].min())) or ((i+1 == num_slices) and (current_trend > slice_quantile['Average'].max()))
                        if trend_boolean:
                            for j in range(num_slices):                        
                                if vol_source == 'BBG':
                                    vols_sub_trend_df = prices.loc[trend_mask]
                                    slice_range = vols_sub_trend_df[u, '{} IV'.format(iv_slice_mat)].dropna().quantile([j/num_slices, (j+1)/num_slices])
                                    slice_quantile = vols_sub_trend_df[(vols_sub_trend_df[u, '{} IV'.format(iv_slice_mat)] > slice_range.values[0]) & (vols_sub_trend_df[u, '{} IV'.format(iv_slice_mat)] <= slice_range.values[1])][u, '{} IV'.format(iv_slice_mat)]
                                else:
                                    vols_mask_df = vols[undl_dict[u], iv_slice_mat, 100].loc[vol_mask_dates]
                                    trend_mask = vols_mask_df.index.intersection(trend_mask)
                                    vols_sub_trend_df = vols.loc[trend_mask].dropna()
                                    slice_range = vols_sub_trend_df[undl_dict[u], iv_slice_mat, 100].dropna().quantile([j/num_slices, (j+1)/num_slices])
                                    slice_quantile = vols_sub_trend_df[(vols_sub_trend_df[undl_dict[u], iv_slice_mat, 100] > slice_range.values[0]) & (vols_sub_trend_df[undl_dict[u], iv_slice_mat, 100] <= slice_range.values[1])][undl_dict[u], iv_slice_mat, 100]

                                vol_mask = slice_quantile.index
                                
                                vol_boolean = ((current_vol > slice_range.values[0]) and (current_vol <= slice_range.values[1])) or ((j == 0) and (current_vol <= slice_range.values[0])) or ((j+1 == num_slices) and (current_vol > slice_range.values[1]))
                                if vol_boolean:
                                    mask = vol_mask
                                    intersect_1d = summary_funct(spot_ranges[u]['Future 1D'].loc[mask].dropna())
                                    intersect_2d = summary_funct(spot_ranges[u]['Future 2D'].loc[mask].dropna())
                                    intersect_3d = summary_funct(spot_ranges[u]['Future 3D'].loc[mask].dropna())
                                    intersect_5d = summary_funct(spot_ranges[u]['Future 5D'].loc[mask].dropna())
                                    intersect_10d = summary_funct(spot_ranges[u]['Future 10D'].loc[mask].dropna())
                                    rv_10d = summary_funct(prices[u, 'Future 10D RV'].loc[mask].dropna()) / np.sqrt(252)
                                    rv_22d = summary_funct(prices[u, 'Future 22D RV'].loc[mask].dropna()) / np.sqrt(252)
                                    range_dict = {'1D': intersect_1d, '2D': intersect_2d, '3D': intersect_3d, '5D': intersect_5d, '10D': intersect_10d}
                                    largest_range = max(range_dict.items(), key=operator.itemgetter(1))[0]
                                    largest_range_dist = spot_ranges[u]['Future {}'.format(largest_range)].loc[mask].dropna()
                                    stats_skew = stats.skew(largest_range_dist.values)
                                    stats_kurt = stats.kurtosis(largest_range_dist.values)
                                    intersect_trend_quantile = i
                                    intersect_vol_quantile = j
                                    summary_trendvol_df = summary_trendvol_df.append(pd.DataFrame({'1D': intersect_1d,
                                                                              '2D': intersect_2d,
                                                                              '3D': intersect_3d,
                                                                              '5D': intersect_5d,
                                                                              '10D': intersect_10d,
                                                                              'RV 10D': rv_10d,
                                                                              'RV 22D': rv_22d,
                                                                              'Biggest Range Skew': stats_skew,
                                                                              'Biggest Range Kurtosis': stats_kurt,       
                                                                              'Trend Quantile': intersect_trend_quantile,
                                                                              'Vol Quantile within Trend Quantile': intersect_vol_quantile,
                                                                              'Count': len(spot_ranges[u]['Future 1D'].loc[mask].dropna())}, index=['{} - {}'.format(start_date, end_date)]))

                    #Calculate Vol Bucketing then Trend Bucketing Data Frame
                    for i in range(num_slices):
                        if vol_source == 'BBG':
                            sub_vols_df = prices.loc[vol_mask_dates]
                            slice_range = sub_vols_df[u, '{} IV'.format(iv_slice_mat)].dropna().quantile([i/num_slices, (i+1)/num_slices])
                            slice_quantile = sub_vols_df[(sub_vols_df [u, '{} IV'.format(iv_slice_mat)] > slice_range.values[0]) & (sub_vols_df [u, '{} IV'.format(iv_slice_mat)] <= slice_range.values[1])][u, '{} IV'.format(iv_slice_mat)]
                        else:
                            sub_vols_df = vols.loc[vol_mask_dates]
                            slice_range = sub_vols_df[undl_dict[u], iv_slice_mat, 100].dropna().quantile([i/num_slices, (i+1)/num_slices])
                            slice_quantile = sub_vols_df[(sub_vols_df[undl_dict[u], iv_slice_mat, 100] > slice_range.values[0]) & (sub_vols_df[undl_dict[u], iv_slice_mat, 100] <= slice_range.values[1])][undl_dict[u], iv_slice_mat, 100]
                        vol_mask = slice_quantile.index
                        vol_boolean = ((current_vol > slice_quantile.min()) and (current_vol <= slice_quantile.max())) or ((i == 0) and (current_vol <= slice_quantile.min())) or ((i+1 == num_slices) and (current_vol > slice_quantile.max()))
                        if vol_boolean:
                            for j in range(num_slices):
                                vol_mask = r_dict[u].loc[trend_mask_dates].index.intersection(vol_mask)
                                trend_sub_vols_df = r_dict[u].loc[vol_mask]
                                slice_range = trend_sub_vols_df['Average'].dropna().quantile([j / num_slices, (j + 1) / num_slices])
                                slice_quantile = trend_sub_vols_df[(trend_sub_vols_df['Average'] > slice_range.values[0]) & (trend_sub_vols_df['Average'] <= slice_range.values[1])]           
                                trend_mask = slice_quantile.index
                               
                                trend_boolean = ((current_trend > slice_range.values[0]) and (current_trend <= slice_range.values[1])) or ((j == 0) and (current_trend <= slice_range.values[0])) or ((j+1 == num_slices) and (current_trend > slice_range.values[1]))
                                if trend_boolean:
                                    mask = trend_mask
                                    intersect_1d = summary_funct(spot_ranges[u]['Future 1D'].loc[mask].dropna())
                                    intersect_2d = summary_funct(spot_ranges[u]['Future 2D'].loc[mask].dropna())
                                    intersect_3d = summary_funct(spot_ranges[u]['Future 3D'].loc[mask].dropna())
                                    intersect_5d = summary_funct(spot_ranges[u]['Future 5D'].loc[mask].dropna())
                                    intersect_10d = summary_funct(spot_ranges[u]['Future 10D'].loc[mask].dropna())
                                    rv_10d = summary_funct(prices[u, 'Future 10D RV'].loc[mask].dropna()) / np.sqrt(252)
                                    rv_22d = summary_funct(prices[u, 'Future 22D RV'].loc[mask].dropna()) / np.sqrt(252)
                                    largest_range = max(range_dict.items(), key=operator.itemgetter(1))[0]
                                    largest_range_dist = spot_ranges[u]['Future {}'.format(largest_range)].loc[mask].dropna()
                                    stats_skew = stats.skew(largest_range_dist.values)
                                    stats_kurt = stats.kurtosis(largest_range_dist.values)                                    
                                    intersect_vol_quantile = i
                                    intersect_trend_quantile = j
                                    summary_voltrend_df = summary_voltrend_df.append(pd.DataFrame({'1D': intersect_1d,
                                                                              '2D': intersect_2d,
                                                                              '3D': intersect_3d,
                                                                              '5D': intersect_5d,
                                                                              '10D': intersect_10d,
                                                                              'RV 10D': rv_10d,
                                                                              'RV 22D': rv_22d,
                                                                              'Biggest Range Skew': stats_skew,
                                                                              'Biggest Range Kurtosis': stats_kurt,                                                                              
                                                                              'Vol Quantile': intersect_vol_quantile,
                                                                              'Trend Quantile within Vol Quantile': intersect_trend_quantile,
                                                                              'Count': len(spot_ranges[u]['Future 1D'].loc[mask].dropna())}, index=['{} - {}'.format(start_date, end_date)]))

            messageHTML += '<H3>{}, {}</H3>'.format(u, bbg_to_name_dict[u])
            messageHTML += 'Past Year Spot Percentile: {},      % to 1Y Spot Max: {}%,      % to 1Y Spot Min: {}%'.format(round(pctile, 2), round(high_pct, 2), round(low_pct, 2))
            messageHTML += '<BR>'
            #Spot Ranges and Strangle Data
            if ~np.isnan(current_vol):
                straddle_price = current_vol * 0.8
                spot_range_max_min = max(abs(high_pct), abs(low_pct))
                messageHTML += '1Y Straddle Price: {},      Max(% to 1Y Spot High, % to 1Y Spot Low): {}'.format(round(straddle_price, 2), round(spot_range_max_min, 2))
                messageHTML += '<BR>'

            messageHTML += 'LFEV 10D: {}, LFEV 22D: {}, Var Ratio 10D: {}, Var Ratio 22D: {}'.format(lfev_10d, lfev_22d, var_ratio_10d, var_ratio_22d)
            messageHTML += '<BR>'
            u_ma_deals = get_live_ma_deals(ma_deals, u, min_deal_size = 1e10)
            has_ma_deal = False
            if len(u_ma_deals) > 0:
                messageHTML += str(u_ma_deals.to_html())
                messageHTML += '<BR>'
                has_ma_deal = True

            #Display Trend DataFrame
            messageHTML += '<H4>{}</H4>'.format('Trend Bucketing')
            summary_trend_df = summary_trend_df.append(summary_trend_df.mean(skipna=True, axis=0).rename('Average'))
            # Summarize the overall bucketing type ranges
            bucket_avg_ranges = pd.DataFrame()
            avg_ranges = summary_trend_df.loc['Average'].to_dict()
            avg_ranges['Weight'] = 3
            bucket_avg_ranges = bucket_avg_ranges.append(pd.DataFrame(avg_ranges, index=['Trend']))

            max_range = summary_trend_df[['1D', '2D', '3D', '5D', '10D']].idxmax(axis=1)
            min_range = summary_trend_df[['1D', '2D', '3D', '5D', '10D']].idxmin(axis=1)
            summary_trend_df['Biggest Range'] = max_range
            summary_trend_df['Smallest Range'] = min_range
            trend_avg_quantile = summary_trend_df.loc['Average']['Quantile']
            trend_hedge_range = summary_trend_df.loc['Average']['Biggest Range']
            trend_hedge_target = summary_trend_df.loc['Average'][trend_hedge_range] * np.sqrt(int(trend_hedge_range.replace('D','')))
            trend_hedge_target_vol = summary_trend_df.loc['Average'][trend_hedge_range] * np.sqrt(252)
            biggest_range_skew = summary_trend_df.loc['Average']['Biggest Range Skew']
            biggest_range_kurt = summary_trend_df.loc['Average']['Biggest Range Kurtosis']

            if ~np.isnan(current_vol):
              iv_to_trend_range_comp_df = iv_to_trend_range_comp_df.append(pd.DataFrame({'{} IV'.format(iv_slice_mat): round(current_vol, 2), 
                                                                    'Trend Bucketing Range Vol': round(trend_hedge_target_vol, 2),
                                                                    'Bucketing Vol - IV': round(trend_hedge_target_vol - current_vol, 2),
                                                                    'Biggest Range Period': trend_hedge_range,
                                                                    'Biggest Range Skew': round(biggest_range_skew, 2),
                                                                    'Biggest Range Kurtosis': round(biggest_range_kurt, 2),
                                                                    'Average Trend Quantile': trend_avg_quantile,
                                                                    '1Y Spot Percentile': round(pctile, 2),
                                                                    '% to 1Y Spot High': round(high_pct, 2),
                                                                    '% to 1Y Spot Low': round(low_pct, 2),
                                                                    'Has MA Deal?': has_ma_deal,
                                                                    'Full Name': bbg_to_name_dict[u],
                                                                    'LFEV 10D': lfev_10d,
                                                                    'LFEV 22D': lfev_22d,
                                                                    'Var Ratio 10D': var_ratio_10d,
                                                                    'Var Ratio 22D': var_ratio_22d}, index=[u]))
            messageHTML += '{} Current Trend: {}%. Average Quantile: {}'.format(end_date.strftime('%D'), round(current_trend, 2),round(summary_trend_df.loc['Average']['Quantile'], 2))
            messageHTML += '<BR>'
            messageHTML += 'Hedge Target: {}% every {}'.format(round(trend_hedge_target, 1), trend_hedge_range)     
            df = summary_trend_df[['1D', '2D', '3D', '5D', '10D', 'RV 10D', 'RV 22D', 'Biggest Range', 'Smallest Range', 'Biggest Range Skew', 'Biggest Range Kurtosis', 'Quantile', 'Count']].to_html()
            ds = str(df)
            messageHTML += ds
            messageHTML += '<BR>'
            

            #Display Vol and Trend/Vol Data Frame and Vol Slope
            if ~np.isnan(current_vol):
                avg_vol_slope = vol_slope_df['{} Range Slope'.format(vol_slope_day)].mean()
                vol_slope_df = vol_slope_df.append(vol_slope_df.mean(skipna=True, axis=0).rename('Average'))
                vol_slope_dict[u] = vol_slope_df  

                messageHTML += '<H4>{}</H4>'.format('Vol Bucketing')
                summary_vol_df = summary_vol_df.append(summary_vol_df.mean(skipna=True, axis=0).rename('Average'))
                avg_ranges = summary_vol_df.loc['Average'].to_dict()
                avg_ranges['Weight'] = 3
                bucket_avg_ranges = bucket_avg_ranges.append(pd.DataFrame(avg_ranges, index=['Vol']))

                max_range = summary_vol_df[['1D', '2D', '3D', '5D', '10D']].idxmax(axis=1)
                min_range = summary_vol_df[['1D', '2D', '3D', '5D', '10D']].idxmin(axis=1)
                summary_vol_df['Biggest Range'] = max_range
                summary_vol_df['Smallest Range'] = min_range
                vol_avg_quantile = summary_vol_df.loc['Average']['Quantile']
                if vol_avg_quantile <= low_vol_cutoff:
                    low_vol_list.append(u)
                vol_hedge_range = summary_vol_df.loc['Average']['Biggest Range']
                vol_hedge_target = summary_vol_df.loc['Average'][vol_hedge_range] * np.sqrt(int(vol_hedge_range.replace('D','')))
                vol_hedge_target_vol = summary_vol_df.loc['Average'][vol_hedge_range] * np.sqrt(252)
                biggest_range_skew = summary_vol_df.loc['Average']['Biggest Range Skew']
                biggest_range_kurt = summary_vol_df.loc['Average']['Biggest Range Kurtosis']

                iv_to_iv_range_comp_df = iv_to_iv_range_comp_df.append(pd.DataFrame({'{} IV'.format(iv_slice_mat): round(current_vol, 2), 
                                                                      'Vol Bucketing Range Vol': round(vol_hedge_target_vol, 2),
                                                                      'Bucketing Vol - IV': round(vol_hedge_target_vol - current_vol, 2),
                                                                      'Biggest Range Period': vol_hedge_range,
                                                                      'Biggest Range Skew': round(biggest_range_skew, 2),
                                                                      'Biggest Range Kurtosis': round(biggest_range_kurt, 2),
                                                                      'Average Vol Quantile': vol_avg_quantile,
                                                                      '1Y Spot Percentile': round(pctile, 2),
                                                                      '% to 1Y Spot High': round(high_pct, 2),
                                                                      '% to 1Y Spot Low': round(low_pct, 2),
                                                                      'Has MA Deal?': has_ma_deal,
                                                                      'Full Name': bbg_to_name_dict[u],
                                                                      'LFEV 10D': lfev_10d,
                                                                      'LFEV 22D': lfev_22d,
                                                                      'Var Ratio 10D': var_ratio_10d,
                                                                      'Var Ratio 22D': var_ratio_22d,
                                                                      'Average {} Range Vol Slope'.format(vol_slope_day): avg_vol_slope}, index=[u]))
                messageHTML += '{} Current {} {} Vol: {}%. Average Quantile: {}'.format(current_vol_date.strftime('%D'), vol_source, iv_slice_mat, round(current_vol, 2),round(summary_vol_df.loc['Average']['Quantile'], 2))
                messageHTML += '<BR>'
                messageHTML += 'Hedge Target: {}% every {}'.format(round(vol_hedge_target, 1), vol_hedge_range)
                df = summary_vol_df[['1D', '2D', '3D', '5D', '10D', 'RV 10D', 'RV 22D', 'Biggest Range', 'Smallest Range', 'Biggest Range Skew', 'Biggest Range Kurtosis', 'Quantile', 'Count']].to_html()
                ds = str(df)
                messageHTML += ds
                messageHTML += '<BR>'

                #Display the slope of the set spot trading range for the vol bucketing
                messageHTML += '<H4>{}</H4>'.format('Vol Bucketing {} Range Slope'.format(vol_slope_day))
                df = vol_slope_dict[u].to_html()
                ds = str(df)
                messageHTML += ds
                messageHTML += '<BR>'


                messageHTML += '<H4>{}</H4>'.format('Trend Bucketing then Vol Bucketing')
                summary_trendvol_df = summary_trendvol_df.append(summary_trendvol_df.mean(skipna=True, axis=0).rename('Average'))
                avg_ranges = summary_trendvol_df.loc['Average'].to_dict()
                avg_ranges['Weight'] = 1
                bucket_avg_ranges = bucket_avg_ranges.append(pd.DataFrame(avg_ranges, index=['Trend/Vol']))

                max_range = summary_trendvol_df[['1D', '2D', '3D', '5D', '10D']].idxmax(axis=1)
                min_range = summary_trendvol_df[['1D', '2D', '3D', '5D', '10D']].idxmin(axis=1)
                summary_trendvol_df['Biggest Range'] = max_range
                summary_trendvol_df['Smallest Range'] = min_range
                intersect_hedge_range = summary_trendvol_df.loc['Average']['Biggest Range']
                intersect_hedge_target = summary_trendvol_df.loc['Average'][intersect_hedge_range] * np.sqrt(int(intersect_hedge_range.replace('D','')))
                intersect_hedge_target_vol = summary_trendvol_df.loc['Average'][intersect_hedge_range] * np.sqrt(252)
                biggest_range_skew = summary_trendvol_df.loc['Average']['Biggest Range Skew']
                biggest_range_kurt = summary_trendvol_df.loc['Average']['Biggest Range Kurtosis']

                iv_to_trendvol_range_comp_df = iv_to_trendvol_range_comp_df.append(pd.DataFrame({'{} IV'.format(iv_slice_mat): round(current_vol, 2), 
                                                                      'Trend and Vol Bucketing Range Vol': round(intersect_hedge_target_vol, 2),
                                                                      'Bucketing Vol - IV': round(intersect_hedge_target_vol - current_vol, 2),
                                                                      'Biggest Range Period': intersect_hedge_range,
                                                                      'Biggest Range Skew': round(biggest_range_skew, 2),
                                                                      'Biggest Range Kurtosis': round(biggest_range_kurt, 2),
                                                                      '1Y Spot Percentile': round(pctile, 2),
                                                                      '% to 1Y Spot High': round(high_pct, 2),
                                                                      '% to 1Y Spot Low': round(low_pct, 2),
                                                                      'LFEV 10D': lfev_10d,
                                                                      'LFEV 22D': lfev_22d,
                                                                      'Var Ratio 10D': var_ratio_10d,
                                                                      'Var Ratio 22D': var_ratio_22d,
                                                                      'Has MA Deal?': has_ma_deal,
                                                                          'Full Name': bbg_to_name_dict[u]}, index=[u]))
                messageHTML += '{} Current Trend: {}%. Average Quantile: {}'.format(end_date.strftime('%D'), round(current_trend, 2),round(summary_trendvol_df.loc['Average']['Trend Quantile'], 2))
                messageHTML += '<BR>'
                messageHTML += '{} Current {} Vol: {}%. Average Quantile: {}'.format(current_vol_date.strftime('%D'), vol_source, round(current_vol, 2),round(summary_trendvol_df.loc['Average']['Vol Quantile within Trend Quantile'], 2))
                messageHTML += '<BR>'
                messageHTML += 'Hedge Target: {}% every {}'.format(round(intersect_hedge_target, 1), intersect_hedge_range)
                df = summary_trendvol_df[['1D', '2D', '3D', '5D', '10D', 'RV 10D', 'RV 22D', 'Biggest Range', 'Smallest Range', 'Biggest Range Skew', 'Biggest Range Kurtosis', 'Trend Quantile', 'Vol Quantile within Trend Quantile', 'Count']].to_html()
                ds = str(df)
                messageHTML += ds
                messageHTML += '<BR>'

                messageHTML += '<H4>{}</H4>'.format('Vol Bucketing then Trend Bucketing')
                summary_voltrend_df = summary_voltrend_df.append(summary_voltrend_df.mean(skipna=True, axis=0).rename('Average'))
                avg_ranges = summary_voltrend_df.loc['Average'].to_dict()
                avg_ranges['Weight'] = 1
                bucket_avg_ranges = bucket_avg_ranges.append(pd.DataFrame(avg_ranges, index=['Vol/Trend']))

                max_range = summary_voltrend_df[['1D', '2D', '3D', '5D', '10D']].idxmax(axis=1)
                min_range = summary_voltrend_df[['1D', '2D', '3D', '5D', '10D']].idxmin(axis=1)
                summary_voltrend_df['Biggest Range'] = max_range
                summary_voltrend_df['Smallest Range'] = min_range
                intersect_hedge_range = summary_voltrend_df.loc['Average']['Biggest Range']
                intersect_hedge_target = summary_voltrend_df.loc['Average'][intersect_hedge_range] * np.sqrt(int(intersect_hedge_range.replace('D','')))
                intersect_hedge_target_vol = summary_voltrend_df.loc['Average'][intersect_hedge_range] * np.sqrt(252)
                biggest_range_skew = summary_voltrend_df.loc['Average']['Biggest Range Skew']
                biggest_range_kurt = summary_voltrend_df.loc['Average']['Biggest Range Kurtosis']

                iv_to_voltrend_range_comp_df = iv_to_voltrend_range_comp_df.append(pd.DataFrame({'{} IV'.format(iv_slice_mat): round(current_vol, 2), 
                                                                      'Trend and Vol Bucketing Range Vol': round(intersect_hedge_target_vol, 2),
                                                                      'Bucketing Vol - IV': round(intersect_hedge_target_vol - current_vol, 2),
                                                                      'Biggest Range Period': intersect_hedge_range,
                                                                      'Biggest Range Skew': round(biggest_range_skew, 2),
                                                                      'Biggest Range Kurtosis': round(biggest_range_kurt, 2),
                                                                      '1Y Spot Percentile': round(pctile, 2),
                                                                      '% to 1Y Spot High': round(high_pct, 2),
                                                                      '% to 1Y Spot Low': round(low_pct, 2),
                                                                      'LFEV 10D': lfev_10d,
                                                                      'LFEV 22D': lfev_22d,
                                                                      'Var Ratio 10D': var_ratio_10d,
                                                                      'Var Ratio 22D': var_ratio_22d,
                                                                      'Has MA Deal?': has_ma_deal,
                                                                          'Full Name': bbg_to_name_dict[u]}, index=[u]))
                messageHTML += '{} Current {} Vol: {}%. Average Quantile: {}'.format(current_vol_date.strftime('%D'), vol_source, round(current_vol, 2),round(summary_voltrend_df.loc['Average']['Vol Quantile'], 2))
                messageHTML += '<BR>'
                messageHTML += '{} Current Trend: {}%. Average Quantile: {}'.format(end_date.strftime('%D'), round(current_trend, 2),round(summary_voltrend_df.loc['Average']['Trend Quantile within Vol Quantile'], 2))
                messageHTML += '<BR>'
                messageHTML += 'Hedge Target: {}% every {}'.format(round(intersect_hedge_target, 1), intersect_hedge_range)
                df = summary_voltrend_df[['1D', '2D', '3D', '5D', '10D', 'RV 10D', 'RV 22D', 'Biggest Range', 'Smallest Range', 'Biggest Range Skew', 'Biggest Range Kurtosis', 'Vol Quantile', 'Trend Quantile within Vol Quantile', 'Count']].to_html()
                ds = str(df)
                messageHTML += ds
                messageHTML += '<BR>'

                strad_vs_move = straddle_price - spot_range_max_min
                iv_to_spot_range_comp_df = iv_to_spot_range_comp_df.append(pd.DataFrame({'{} IV'.format(iv_slice_mat): round(current_vol, 2),
                                                                    'Average Vol Quantile': vol_avg_quantile,  
                                                                    '1Y Straddle Price': straddle_price,
                                                                    '1Y Spot Percentile': round(pctile, 2),
                                                                    '% to 1Y Spot High': round(high_pct, 2),
                                                                    '% to 1Y Spot Low': round(low_pct, 2),
                                                                    'LFEV 10D': lfev_10d,
                                                                    'LFEV 22D': lfev_22d,
                                                                    'Var Ratio 10D': var_ratio_10d,
                                                                    'Var Ratio 22D': var_ratio_22d,
                                                                    'Has MA Deal?': has_ma_deal,
                                                                    'Straddle - Max(% to 1Y Spot High, % to 1Y Spot Low)': strad_vs_move,
                                                                    'Full Name': bbg_to_name_dict[u]}, index=[u]))

            messageHTML += '<H4>Summary Ranges of All Bucketing Methods</H4>'
            day_columns = ['1D', '2D', '3D', '5D', '10D']
            weighted_avg_ranges = np.average(bucket_avg_ranges[day_columns], axis=0, weights=bucket_avg_ranges['Weight'])
            bucket_avg_ranges = bucket_avg_ranges.append(pd.DataFrame([list(weighted_avg_ranges)], columns = day_columns, index=['Wgted Average']))
            max_range = bucket_avg_ranges[day_columns].idxmax(axis=1)
            min_range = bucket_avg_ranges[day_columns].idxmin(axis=1)
            bucket_avg_ranges['Biggest Range'] = max_range
            bucket_avg_ranges['Smallest Range'] = min_range
            wgt_avg_hedge_range = bucket_avg_ranges.loc['Wgted Average']['Biggest Range']
            wgt_avg_hedge_target = bucket_avg_ranges.loc['Wgted Average'][wgt_avg_hedge_range] * np.sqrt(int(wgt_avg_hedge_range.replace('D','')))
            messageHTML += 'Weighted Average Hedge Target: {}% every {}'.format(round(wgt_avg_hedge_target, 1), wgt_avg_hedge_range)
            messageHTML += '<BR>'
            df = bucket_avg_ranges[day_columns+['Biggest Range', 'Weight']].to_html()
            ds = str(df)
            messageHTML += ds
            messageHTML += '<BR><BR>'

            if np.isnan(current_vol):
                messageHTML += 'No current vol'
                messageHTML += '<BR>'

        iv_to_trend_range_comp_df['Rank'] = iv_to_trend_range_comp_df['Bucketing Vol - IV'].rank(ascending=False)
        iv_to_iv_range_comp_df['Rank'] = iv_to_iv_range_comp_df['Bucketing Vol - IV'].rank(ascending=False)
        iv_to_iv_range_comp_df['{} Range Vol Slope Rank'.format(vol_slope_day)] = iv_to_iv_range_comp_df['Average {} Range Vol Slope'.format(vol_slope_day)].rank(ascending=False)
        iv_to_trendvol_range_comp_df['Rank'] = iv_to_trendvol_range_comp_df['Bucketing Vol - IV'].rank(ascending=False)
        iv_to_voltrend_range_comp_df['Rank'] = iv_to_voltrend_range_comp_df['Bucketing Vol - IV'].rank(ascending=False)
        iv_to_spot_range_comp_df['Rank'] = iv_to_spot_range_comp_df['Straddle - Max(% to 1Y Spot High, % to 1Y Spot Low)'].rank(ascending=True)

        summary_rank_df = pd.concat([iv_to_trend_range_comp_df['Rank'].to_frame('Trend Bucketing'), 
                                      iv_to_iv_range_comp_df['Rank'].to_frame('Vol Bucketing'),
                                      iv_to_iv_range_comp_df['{} Range Vol Slope Rank'.format(vol_slope_day)].to_frame('{} Range Vol Slope Rank'.format(vol_slope_day)),
                                      iv_to_trendvol_range_comp_df['Rank'].to_frame('Trend Bucketing then Vol Bucketing'),
                                      iv_to_voltrend_range_comp_df['Rank'].to_frame('Vol Bucketing then Trend Bucketing'),
                                      iv_to_spot_range_comp_df['Rank'].to_frame('1Y Straddle vs 1Y Spot Range'),
                                      iv_to_trend_range_comp_df['1Y Spot Percentile'].to_frame('1Y Spot Percentile'),
                                      iv_to_trend_range_comp_df['% to 1Y Spot High'].to_frame('% to 1Y Spot High'),
                                      iv_to_trend_range_comp_df['% to 1Y Spot Low'].to_frame('% to 1Y Spot Low'),
                                      iv_to_trend_range_comp_df['Has MA Deal?'].to_frame('Has MA Deal?'),
                                      iv_to_trend_range_comp_df['Full Name'].to_frame('Full Name'),
                                      iv_to_iv_range_comp_df['Average Vol Quantile'].to_frame('Avg Vol Quantile'),
                                      iv_to_iv_range_comp_df['{} IV'.format(iv_slice_mat)].to_frame('{} IV'.format(iv_slice_mat)),
                                      iv_to_trend_range_comp_df['LFEV 10D'].to_frame('LFEV 10D'),
                                      iv_to_trend_range_comp_df['LFEV 22D'].to_frame('LFEV 22D'),
                                      iv_to_trend_range_comp_df['Var Ratio 10D'].to_frame('Var Ratio 10D'),
                                      iv_to_trend_range_comp_df['Var Ratio 22D'].to_frame('Var Ratio 22D'),
                                      ], axis=1)
        summary_rank_df['All Average Rank'] = summary_rank_df[['Trend Bucketing', 'Vol Bucketing', 'Trend Bucketing then Vol Bucketing', 'Vol Bucketing then Trend Bucketing']].mean(axis=1)
        summary_rank_df['Vol, Trend, and Spot Range Average Rank'] = summary_rank_df[['Trend Bucketing', 'Vol Bucketing', '1Y Straddle vs 1Y Spot Range']].mean(axis=1)
        summary_rank_df['Vol and Trend Only Average Rank']  = summary_rank_df[['Trend Bucketing', 'Vol Bucketing']].mean(axis=1)
        summary_rank_df = summary_rank_df.sort_values('Vol, Trend, and Spot Range Average Rank', ascending=True)
        
        spot_stats_columns = ['1Y Spot Percentile', '% to 1Y Spot High', '% to 1Y Spot Low', 
        'LFEV 10D', 'LFEV 22D', 'Var Ratio 10D', 'Var Ratio 22D', 
        'Has MA Deal?']

        messageSummaryHTML += '<H2>{}</H2>'.format('Summary Rankings')
        messageSummaryHTML += '<H4>Trend Returns {} </H4>'.format(auto_corr_len)
        column_order = ['Full Name', 'Vol, Trend, and Spot Range Average Rank', 
                        'Vol and Trend Only Average Rank', 'All Average Rank', '1Y Straddle vs 1Y Spot Range', 
                        'Trend Bucketing', 'Vol Bucketing', 'Trend Bucketing then Vol Bucketing', 
                        'Vol Bucketing then Trend Bucketing', '{} Range Vol Slope Rank'.format(vol_slope_day), 'Avg Vol Quantile'] + spot_stats_columns
        df = summary_rank_df[column_order].to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR>'

        messageSummaryHTML += '<H2>Low Vol <={} Decile Ranking</H2>'.format(low_vol_cutoff)
        low_vol_summary_rank_df = summary_rank_df.loc[low_vol_list].sort_values('Vol, Trend, and Spot Range Average Rank', ascending=True)
        df = low_vol_summary_rank_df[column_order].to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR>'

        messageSummaryHTML += '<H2>{}</H2>'.format('Straddle vs Spot Range Summary')
        column_order = ['Full Name', '{} IV'.format(iv_slice_mat), 'Average Vol Quantile', '1Y Straddle Price', 'Straddle - Max(% to 1Y Spot High, % to 1Y Spot Low)'] + spot_stats_columns
        df = iv_to_spot_range_comp_df[column_order].sort_values('Straddle - Max(% to 1Y Spot High, % to 1Y Spot Low)', ascending=True).to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR>'

        messageSummaryHTML += '<H2>{}</H2>'.format('Trend Bucketing Summary')
        column_order = ['Full Name', '{} IV'.format(iv_slice_mat), 'Trend Bucketing Range Vol', 'Bucketing Vol - IV', 'Biggest Range Period', 'Biggest Range Skew', 'Biggest Range Kurtosis', 'Average Trend Quantile'] + spot_stats_columns
        df = iv_to_trend_range_comp_df[column_order].sort_values('Bucketing Vol - IV', ascending=False).to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR>'

        messageSummaryHTML += '<H2>{}</H2>'.format('Vol Bucketing Summary')
        column_order = ['Full Name', '{} IV'.format(iv_slice_mat), 'Vol Bucketing Range Vol', 'Bucketing Vol - IV', 'Biggest Range Period', 'Biggest Range Skew', 
                        'Biggest Range Kurtosis', 'Average Vol Quantile', '{} Range Vol Slope Rank'.format(vol_slope_day)] + spot_stats_columns
        df = iv_to_iv_range_comp_df[column_order].sort_values('Bucketing Vol - IV', ascending=False).to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR>'

        messageSummaryHTML += '<H2>{}</H2>'.format('Trend Bucketing then Vol Bucketing Summary')
        column_order = ['Full Name', '{} IV'.format(iv_slice_mat), 'Trend and Vol Bucketing Range Vol', 'Bucketing Vol - IV', 'Biggest Range Period', 'Biggest Range Skew', 'Biggest Range Kurtosis'] + spot_stats_columns
        df = iv_to_trendvol_range_comp_df[column_order].sort_values('Bucketing Vol - IV', ascending=False).to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR>'

        messageSummaryHTML += '<H2>{}</H2>'.format('Vol Bucketing then Trend Bucketing Summary')
        column_order = ['Full Name', '{} IV'.format(iv_slice_mat), 'Trend and Vol Bucketing Range Vol', 'Bucketing Vol - IV', 'Biggest Range Period', 'Biggest Range Skew', 'Biggest Range Kurtosis'] + spot_stats_columns
        df = iv_to_voltrend_range_comp_df[column_order].sort_values('Bucketing Vol - IV', ascending=False).to_html()
        ds = str(df)
        messageSummaryHTML += ds
        messageSummaryHTML += '<BR><BR><BR>'


        messageHTML = messageSummaryHTML + messageHTML
        msg.attach(MIMEText(messageHTML, 'html'))


        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
            server.login(sender_email, password)
            message = msg.as_string()
            server.sendmail(sender_email, receiver_email, message)
            server.quit()

if __name__ == '__main__':
    main()

